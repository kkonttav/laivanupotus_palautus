<?php
session_start();
session_regenerate_id();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Laivanupotus</title>
        <style type="text/css">
            table {border-collapse: collapse; text-align: center;}
            td {width: 20px; border: solid 1px black;}
        </style>
    </head>
    <body>
        <h3>Laivanupotus-peli</h3>
        <?php
        define("KOORDINAATTI","O");
        define("LAIVA","X");
        define("HUTIKUTI","-");
        define("RIVEJA",5);
        define("SARAKKEITA",5);
        define("LAIVOJA",5);

        // Onko uusi peli alkamassa
        if(!isset($_SESSION["pelialoitettu"])) {
            $_SESSION["pelialoitettu"]=true;
            $_SESSION["osumat"]=0;
            $arvottuja=0; 
            $luettu=false;
        
            $pelilauta=array(array());
            $laivat=array(array());
        
            // Täytä pelilauta ja laivat lauta puhtaaksi
            for($i=0;$i<SARAKKEITA;$i++) {
                for($j=0;$j<RIVEJA;$j++) {
                    $pelilauta[$i][$j]=KOORDINAATTI;
                    $laivat[$i][$j]=KOORDINAATTI;
                }
            }
            // Arvo laivat paikoilleen
            while($arvottuja<LAIVOJA) {
                $x=rand(0,4);
                $y=rand(0,4);
                if($laivat[$x][$y]!=LAIVA) {
                    $laivat[$x][$y]=LAIVA;
                    $arvottuja++;
                }
            }
            // talleta sessioon pelilauta ja laivat
            $_SESSION["pelilauta"]=$pelilauta;
            $_SESSION["laivat"]=$laivat;
            print "<br />";
        }
        // Ammunta valittu hiirellä "linkistä"
        else if ($_SERVER["REQUEST_METHOD"] == "GET") {
            if($_SESSION["osumat"]<LAIVOJA) {
                $luettu=true;
                $x=filter_input(INPUT_GET,'x',FILTER_SANITIZE_NUMBER_INT);
                $y=filter_input(INPUT_GET,'y',FILTER_SANITIZE_NUMBER_INT);
            }
            else {
                // kaikki löytyneet älä käsittele vaan anna latautua uusi peli
                $luettu=false;
            }                
        }
        // Ammunta valittu antamalla X ja Y koordinaatit formiin
        else {
            $luettu=true;
            $x=filter_input(INPUT_POST,'txtX',FILTER_SANITIZE_NUMBER_INT);
            $y=filter_input(INPUT_POST,'txtY',FILTER_SANITIZE_NUMBER_INT);
         }
         // Onko koordinaatit saatu
         if($luettu==true)
         {
            if($x<SARAKKEITA && $y<RIVEJA) {
                if($_SESSION["pelilauta"][$x][$y]==KOORDINAATTI && $_SESSION["laivat"][$x][$y]==LAIVA) {
                    $_SESSION["osumat"]++;
                    $_SESSION["pelilauta"][$x][$y]=LAIVA;
                    print "Osuit!<br />";
                }
                else if ($_SESSION["pelilauta"][$x][$y]==LAIVA) {
                    print "Olet ampunut jo tämän laivan!<br />";
                }
                else if ($_SESSION["pelilauta"][$x][$y]==HUTIKUTI) {
                    print "Tässä ei ole edelleenkään laivaa!<br />";
                }
                else {
                    $_SESSION["pelilauta"][$x][$y]=HUTIKUTI;
                    print "Ohi meni!<br />";
                }
            }
            else {
                print "Ammuit ohi pelilaudalta, yritä uudestaan!<br />";
            }
        }
        // Tulostetaan pelilauta html-taulukkona
        print "<table>";
        for($i=0;$i<RIVEJA;$i++) {
            print "<tr>";
            for($j=0;$j<SARAKKEITA;$j++) {
                // jos jo merkitty niin piirrä merkintä
                if($_SESSION["pelilauta"][$j][$i]==LAIVA || $_SESSION["pelilauta"][$j][$i]==HUTIKUTI) {
                    print "<td>" . $_SESSION["pelilauta"][$j][$i] . "</td>";                
                }
                else // muuten piirrä linkki
                {
                    print "<td>" . "<a href=index.php?x=$j&y=$i>O</a>" . "</td>";                    
                } 
            }
            print "</tr>";
        }
        print "</table>";
        ?>
        <br />
        <?php
        // Mikäli kaikkia laivoja ei vielä ole löydetty tulosta formi
        if($_SESSION["osumat"]<LAIVOJA) {
        ?>
            <form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
                X: <input name="txtX" size="1" maxlength="1" required />&nbsp;
                Y: <input name="txtY" size="1" maxlength="1" required />
                <input type="submit" value="Ammu" />
            </form>
        <?php
        }
        // Kaikki laivat löydettu nollaa session muuttujat ja anna linkki aloittaa uusi peli
        else {
            print "Kaikki laivat upotettu!<br />";
            print "<a href=" . $_SERVER['PHP_SELF']. ">Uusi peli</a><br />";
            unset($_SESSION["osumat"]);
            unset($_SESSION["pelialoitettu"]);
            unset($_SESSION["laivat"]);
            unset($_SESSION["pelilauta"]);
            session_destroy();
        }
        ?>
    </body>
</html>
